FROM ubuntu:bionic-20180821

RUN apt update -y
RUN apt upgrade -y
RUN apt install -y g++
RUN apt install -y make
RUN apt install -y cmake
RUN apt install -y gcc-mingw-w64
RUN apt install -y g++-mingw-w64
RUN apt install -y clang
RUN apt install -y wine-stable
RUN apt install -y git
RUN apt install -y libsdl2-dev
